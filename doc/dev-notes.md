## lua-luv and vim.loop

This package appeared as an internal module for the nvim-env plugin.
Therefore, at the initial stage I used those things that are available to Lua
code inside nvim.
One of these things was the `luv` library, available as `vim.loop`.

Then, to be able to reuse the code from this module, I moved it into a separate
package - `alogger`.
And at the very beginning, in order for this to work outside of Vim using the
same luv library, I installed this library at the operating system level.
But then I found a way to get away from this dependency of `luv`.

So this is a note from the time when I used the `luv` library here

> Dependencies:
 + Luv (uv) for OS independed way to check directory
  (Alternative for this luafilesystem-pacakge seems it not ships with nvim)
   to work outside of nvim, luv must be installed on your system
   use `luarocks install luv` (required luarocks v3)
    or `apt intall lua-luv`   (for debian)


This is how I used it before:
```lua
local ok_uv, uv = pcall(require, 'luv') -- take from luarocks
if not ok_uv and inside_vim then
  ok_uv = true
  uv = vim.loop -- try to take from nvim
end
```

inside nvim
```lua
local luv = vim.loop
```

outside nvim
```lua
local luv = require('luv') --> /usr/lib/x86_64-linux-gnu/lua/5.1/luv.so
                           -- OS package lua-luv (depends on libuv1)
```
