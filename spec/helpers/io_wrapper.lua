--
-- Goal: Wrap stdio function for testings
--
-- 18-05-2023 @author Swarg
--
local M = {}

local io_orig = {}
local state = {
  filename = '',
  mode = '',
  file = nil, -- fd userdata
  output = {},
  state = ''
}

function M.get_state()
  return state
end

function M.clear_state()
  state.filename = nil
  state.mode = nil
  state.file = nil
  state.output = {}
  state.state = ''
end

local p_open = function(afilename, amode) -- io.open
  assert(type(afilename) == 'string', 'Expected filename')
  assert(type(amode) == 'string', 'Expected mode')
  state.filename = afilename
  state.mode = amode
  state.file = { 'userdata' }
  state.state = 'open'
  return state.file -- file userdata?
end

---@diagnostic disable-next-line: unused-local
local p_output = function(file)
end

local p_write = function(...)
  local line = ''
  for i = 1, select('#', ...) do
    if i > 1 then line = line .. ' ' end
    local val = select(i, ...)
    line = line .. tostring(val)
  end
  table.insert(state.output, line)
end

---@diagnostic disable-next-line: unused-local
local p_close = function(file) -- io.open
  state.state = 'closed'
  table.insert(file, 'closed')
end


function M.wrap()
  io_orig.data = os.date
  io_orig.open = io.open
  io_orig.input = io.input
  io_orig.read = io.read
  io_orig.output = io.output
  io_orig.write = io.write
  io_orig.close = io.close
  io.open = p_open
  io.output = p_output
  io.write = p_write
  io.close = p_close
  io.input = io.input
  io.read = io.read
  return state
end

function M.restore()
  assert(io_orig.open, 'not nil')
  assert(io_orig.read, 'not nil')
  assert(io_orig.write, 'not nil')
  assert(io_orig.close, 'not nil')
  io.open   = io_orig.open
  io.input  = io_orig.input
  io.output = io_orig.output
  io.read   = io_orig.read
  io.write  = io_orig.write
  io.close  = io_orig.close
end

return M
