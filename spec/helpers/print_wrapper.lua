-- 20-10-2023 @author Swarg
local M = {}
-- Goal: testing

local original = {}
M.output_lines = {}


function M.overrided_print(...)
  -- smart varargs handling. build many to one line-string
  local output = ''
  for i = 1, select('#', ...) do
    local val = select(i, ...)
    if i > 1 then
      output = output .. ' '
    end
    if type(val) ~= 'table' then
      output = output .. tostring(val)
    end
  end
  if string.find(output, '\n', 1, true) then
    M.split_range(output, "\n", 0, #output, false, M.output_lines)
    -- local lines = vim.split(output, "\n\r?", { trimempty = false })
    -- vim.list_extend(output_lines, lines, 1, #lines)
  else
    table.insert(M.output_lines, output)
  end
end

function M.add_line(line)
  assert(type(line) == 'string')
  table.insert(M.output_lines, line)
end

function M.get_output()
  return M.output_lines
end

function M.clear_output()
  M.output_lines = {}
end

function M.wrap()
  original.print = _G.print
  _G.print = M.overrided_print
end

function M.restore()
  assert(original.print ~= nil)
  _G.print = original.print
end

return M
