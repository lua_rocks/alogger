require 'busted.runner' ()
local assert = require('luassert')

local M = require("alogger")
local iow = require 'spec.helpers.io_wrapper'
local print_wrapper = require("spec.helpers.print_wrapper")



describe('a-logger backtrace', function()
  --
  local function call_logdebug_deep1()
    local function call_logdebug_deep2()
      local function call_logdebug_deep3()
        M.debug('key: %s backtrace: %s', 64, '::BACKTRACE::') -- lnum: 15
      end
      call_logdebug_deep3()                                   -- lnum: 17
    end
    call_logdebug_deep2()                                     -- lnum: 19
  end

  it("get_trace_path", function()
    local w = iow.wrap()

    assert.no_error(function()
      M.setup({ save = true, level = M.levels.DEBUG, silent_debug = false })
    end)
    M.set_date_format('')
    assert.same(M.levels.DEBUG, M.get_level())
    assert.same(print, M.__get_config().print)

    M.__get_config().print = print_wrapper.overrided_print
    assert.same(print_wrapper.overrided_print, M.__get_config().print)

    call_logdebug_deep1() -- M.debug('message', ':BACKTRACE:') -- lnum: 35

    local lines = w.output
    local exp = "[] [DEBUG] spec/backtrace_spec.lua:15: " .. -- trace from logger
        "key: 64 backtrace:" ..
        " spec/backtrace_spec.lua:17" ..
        " spec/backtrace_spec.lua:19" ..
        " spec/backtrace_spec.lua:35" ..
        " =[C]:-1 \n"
    -- " /usr/local/share/lua/5.1/busted/core.lua:178 \n"

    assert.same(exp, (lines or {})[1])
  end)
end)
