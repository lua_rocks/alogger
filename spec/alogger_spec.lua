require 'busted.runner' ()
local assert = require('luassert')

local M = require("alogger")
local iow = require 'spec.helpers.io_wrapper'
local print_wrapper = require("spec.helpers.print_wrapper")


-- own impl for extend default_config by opts
describe('env.logger_extend.tbls', function()
  it("merge_tbls", function()
    -- default_config  -  values inside not changes! after expend
    local tbl_def = {
      key1 = 'a',
      key2 = 'b',
      key3 = { ksub1 = 'd1', ksub4 = 8 },
      key4 = { ksub01 = 'e0' },
      key5 = { 'a', 'b', 'e' },
      keyQ = 'q',
    }
    -- overrides opts
    local tbl_opts = {
      key2 = 'B',
      key3 = { ksub1 = 'D1', ksub2 = 16, ksub3 = 4 },
      key4 = { ksub02 = 'EE' },
      key5 = { 'c', 'd' },
      key6 = 'newone'
    }
    -- expected result of merge two tables
    local exp = {
      key1 = "a",
      key2 = "B",
      key3 = {
        ksub1 = "D1", ksub2 = 16, ksub3 = 4, ksub4 = 8
      },
      key4 = {
        ksub01 = "e0", ksub02 = "EE"
      },
      key5 = { "c", "d" }, -- key5 from def ovverided by value of tbl_opts.key5
      key6 = 'newone',
      keyQ = "q",
    }
    local defcopy = M.tbl_deep_copy(tbl_def)
    local res = M.tbl_override(defcopy, tbl_opts)
    assert.same(exp, res)
    -- sure tbl_def dont changed
    assert.same('b', tbl_def.key2)
    assert.same(nil, tbl_def.key6)
    assert.same({ 'a', 'b', 'e' }, tbl_def.key5)
    -- sure tbl_opts dont changed
    assert.same(nil, tbl_opts.key1)
    assert.same(nil, tbl_opts.keyQ)
    assert.same({ 'c', 'd' }, tbl_opts.key5)
    -- sure defcopy the new one not tbl_def
    assert.same('B', defcopy.key2)
    assert.same('newone', defcopy.key6)
    -- sure res ref to defcopy
    defcopy.key2 = 'bBb'
    assert.same('bBb', res.key2)
  end)
end)


describe('alogger', function()
  local date_pattern = "%[(%d+)%-(%d+)%-(%d+)%s(%d+):(%d+):(%d+)%]"

  it("date-matching", function()
    local date = '[2023-05-19 12:00:00]'
    assert.matches(date_pattern, date)
  end)

  -- local lvl_pattern = '%s%[[%a%s]+%]%s'
  local lvl_pattern = '%s%[[%a%s]+%]%s'

  it("lvl-matching", function()
    assert.matches(lvl_pattern, ' [TRACE] ')
    assert.matches(lvl_pattern, ' [INFO ] ')
    assert.is_nil(string.match(' [TRA-CE] ', lvl_pattern))    -- not passed
    assert.is_nil(string.match('[TRACE]', lvl_pattern))
    assert.is_not_nil(string.match(' [DEBUG] ', lvl_pattern)) -- passed
  end)

  --local trace_pattern = "@%.[/%w%-_]+/logger%.lua:%d+:%s"
  local trace_pattern = "%.?[/%w%-_]+/alogger_spec%.lua:%d+:%s"
  -- local trace_pattern = "%.lua:%d+:%s"

  it("trace-matching", function()
    local trace = 'spec/alogger_spec.lua:123: '
    assert.matches(trace_pattern, trace)
  end)

  it("msg_prefix-matching", function()
    local pref = '[2023-10-20 10:24:29] [TRACE] ./spec/alogger_spec.lua:142: '
    local pref_ptrn = date_pattern .. lvl_pattern .. trace_pattern
    assert.matches(date_pattern, pref)
    assert.matches(lvl_pattern, pref)
    assert.matches(trace_pattern, pref)
    assert.matches(date_pattern .. lvl_pattern, pref)
    assert.matches(pref_ptrn, pref)
  end)

  local msg_pattern = ":%d+:%s([%w%%%-_%s]+)\n"
  it("msg-matching", function()
    local s = '@./lua/env/logger_spec.lua:123: trace_msg \n'
    assert.same('trace_msg ', string.match(s, msg_pattern))
  end)

  -- Side effect: mkdir for default log-files
  it("setup", function()
    local log_config = M.setup({ save = true })
    assert.same(true, log_config.save)
    assert.is_not_nil(log_config.log_dir)
    assert.is_not_nil(log_config.log_file)
    assert.same('INFO', M.get_level_name())
    assert.same(M.levels.INFO, log_config.level)
    assert.same(M.get_level(), log_config.level)
  end)

  it("mk_short_source", function()
    local source = '@/home/u/dev/app/lua/module/file.lua'
    local app_root = '/home/u/dev/app'
    assert.same("module/file.lua", M.mk_short_source(source, app_root))

    source = '@/home/u/another/f.lua'
    assert.same('/home/u/another/f.lua', M.mk_short_source(source, app_root))

    source = "@./lua/env/logger.lua"
    app_root = '.'
    assert.same('lua/env/logger.lua', M.mk_short_source(source, app_root))

    source = "@test/env/logger_spec.lua"
    app_root = '.'
    assert.same('test/env/logger_spec.lua', M.mk_short_source(source, '.'))
  end)

  it("override print", function()
    -- assert.no_error(function()
    local lines = {}
    print_wrapper.wrap()
    local print0 = function(l) table.insert(lines, l) end
    M.setup({ save = true, level = M.levels.TRACE, print = print0 })
    assert.same(print0, M.__get_config().print)

    M.notify(2, 'Sure a log notify use a print0')
    print('Sure a log notify n not use Global print')
    print_wrapper.restore()
    assert.same({ 'Sure a log notify use a print0' }, lines)

    local exp = { 'Sure a log notify n not use Global print' }
    assert.same(exp, print_wrapper.get_output())
    print_wrapper.clear_output()
  end)


  -- wrap stdio-fumction to check how the logger works
  it("save_message", function()
    local log = M.log
    -- wrap io-funcs and return wrapper state
    local w = iow.wrap()

    assert.no_error(function()
      M.setup({ save = true, level = M.levels.TRACE, silent_debug = false })
    end)
    assert.same(M.levels.TRACE, M.get_level())
    assert.same(print, M.__get_config().print)

    M.__get_config().print = print_wrapper.overrided_print
    assert.same(print_wrapper.overrided_print, M.__get_config().print)

    assert.no_error(function()
      log(M.levels.TRACE, "trace_message")                   -- 1
      log(M.levels.DEBUG, "debug_message")                   -- 2
      log(M.levels.INFO, "info_message")                     -- 3
      log(M.levels.WARN, "warn_message")                     -- 4
      log(M.levels.ERROR, "error_fmt %s %d", "err_code", 16) -- 5
      log(M.levels.FATAL, "formated %s %d", "word", 16)      -- 6
      log(M.levels.FATAL, "formated %s %d", nil, 3, 2)       --broke fmt
      M.trace("trace_message 2")                             -- 8
      M.debug("debug_message 2")                             -- 9
      M.info("info_message 2")                               -- 10
      M.warn("warn_message 2")                               -- 11
      M.error("error_message 2")                             -- 12
      M.fatal("fatal_message 2")                             -- 13
      M.debug("d formated %s %d", 'str', 8)                  -- 14
      M.trace("t formated %s %d", 'str', 9)                  -- 15
      M.info("i formated %s %d", 'str', 10)                  -- 16
    end)
    iow.restore()
    -- '[2023-05-19 12:00:00] [TRACE] @./lua/env/logger.lua:123: trace-message
    local exp_pat = date_pattern .. lvl_pattern .. trace_pattern
    local lines = w.output
    local exp_m1_pattern = exp_pat .. "trace_message" -- for '-' use '%-' !
    assert.same('.', M.get_app_root())

    --assert.same('show-me-out', lines[1])
    assert.matches(exp_m1_pattern, lines[1])
    -- print("[DEBUG] output:", vim.inspect(w))
    assert.same(' [TRACE] ', string.match(lines[1], lvl_pattern))
    assert.same('trace_message', string.match(lines[1], msg_pattern))


    assert.same(' [DEBUG] ', string.match(lines[2], lvl_pattern))
    assert.same('debug_message', string.match(lines[2], msg_pattern))

    assert.same(' [INFO ] ', string.match(lines[3], lvl_pattern))
    assert.same(' [WARN ] ', string.match(lines[4], lvl_pattern))
    assert.same(' [ERROR] ', string.match(lines[5], lvl_pattern))
    assert.same('error_fmt err_code 16', string.match(lines[5], msg_pattern))
    assert.same(' [FATAL] ', string.match(lines[6], lvl_pattern))
    assert.same('formated word 16', string.match(lines[6], msg_pattern))
    assert.same(' [FATAL] ', string.match(lines[7], lvl_pattern))
    assert.same(' [FATAL] ', string.match(lines[7], lvl_pattern))
    assert.same('formated nil 3 2', string.match(lines[7], msg_pattern))
    -- --formated %s %d
    --
    assert.same(' [TRACE] ', string.match(lines[8], lvl_pattern))
    --
    assert.same('d formated str 8', string.match(lines[14], msg_pattern))
    assert.same('t formated str 9', string.match(lines[15], msg_pattern))
    assert.same('i formated str 10', string.match(lines[16], msg_pattern))

    -- deep
    local exp_trace = "[./]*spec/alogger_spec.lua:%d+" -- trace of this test file
    for _, line in pairs(lines) do
      assert.match(exp_trace, line)
    end

    iow.clear_state()
    --
    -- notify output
    local lines2 = print_wrapper.get_output()
    assert.same(16, #lines2) -- silent debug
    assert.matches(exp_m1_pattern, lines2[1])
    print_wrapper.clear_output()
  end)

  --
  it("build_message -- format", function()
    local a_eq = assert.same
    a_eq('msg abc', M.format('msg %s', 'abc'))
    a_eq('msg a', M.format('msg %s', 'a'))
    a_eq('msg {k = "v"}', M.format('msg %s', { k = 'v' }))
    a_eq('msg a {k = "v"}', M.format('msg %s %s', 'a', { k = 'v' }))
    a_eq('msg {k = "v"}', M.format('msg', { k = 'v' }))
    a_eq('msg abc', M.format('msg', 'abc'))
    a_eq('msg abc 0', M.format('msg', 'abc', 0))
    a_eq('msg abc true 1', M.format('msg', 'abc', true, 1))
    a_eq('msg abc nil a', M.format('msg', 'abc', nil, 'a'))
    a_eq('msg nil', M.format('msg %s', nil))
    a_eq('msg nil', M.format('msg %s', 'nil'))
    a_eq('nil', type(nil))
    -- a_eq('X', M.status())
  end)

  it("set_date_format", function()
    -- '[2023-10-19 13:51:02] [INFO ] ./test/env/logger_spec.lua:192: message'
    local exp = '%[[%d%-%s:]+%] %[INFO %] [./]*spec/alogger_spec.lua:%d+: message .'
    M.__get_config().app_root = '.'
    local res = M.build_log_message(2, 2, 'message 1')
    assert.match(exp, res)
    assert.same("\n", res:sub(-1, -1))

    local exp_nodate = '%[%] %[INFO %] [./]*spec/alogger_spec.lua:%d+: message'
    M.set_date_format('')
    assert.match(exp_nodate, M.build_log_message(2, 2, 'message'))
    M.__get_config().app_root = nil
  end)

  it("get_logdir", function()
    local exp = M.join_path(os.getenv('HOME'), 'logs')
    assert.same(exp, M.get_logdir())
  end)

  it("join_path", function()
    assert.same('/', M.join_path(nil, nil))
    assert.same('/', M.join_path('', nil))
    assert.same('/', M.join_path('', ''))
    assert.same('a/b', M.join_path('a', 'b'))
    assert.same('a/b', M.join_path('a/', 'b'))
    assert.same('a/b', M.join_path('a', '/b'))
    assert.same('a/b', M.join_path('a/', '/b'))
  end)

  it("inspect0", function()
    local t1 = {}
    local t2 = { parent = t1 }
    local t3 = { parent = t1 }
    -- recursive refs -- ring
    t1.child1 = t2
    t1.child2 = t3
    local exp = '{parent = {child2 = {parent = <table-2>}, child1 = <table-1>}}'
    assert.same(exp, M.inspect0(t2))
  end)

  it("fast_log_to_mem", function()
    M.fast_log_to_mem()
    -- M.notify(1, 'message')
    local exp = ".DEBUG. .*:%d+: Setup Verbose Output"
    assert.is_table(M.messages) ---@type table
    assert.match(exp, M.messages[1] or '')

    M.debug('test message.')
    assert.match("test message%.", M.messages[#M.messages])

    M.fast_off()
  end)

  it("logger log_to_mem and last_msg", function()
    M.fast_log_to_mem()
    local exp = '1:DEBUG.V., Save: false, File: .* Existed: false .No Date.'
    assert.match(exp, M.status())
    assert.match('Start Logging to Memory', M.last_msg() or '')
    assert.match('Setup Verbose Output', M.last_msg(1) or '')
    assert.is_table(M.messages)
    M.messages = nil -- clear
  end)

  it("logger is_debug is_trace is_level", function()
    M.fast_log_to_mem(M.levels.DEBUG, false)
    local exp = '1:DEBUG.V., Save: false, File: .* Existed: false .No Date.'
    assert.match(exp, M.status())

    assert.same(false, M.is_trace())
    assert.same(true, M.is_debug())
    assert.same(false, M.is_level(M.levels.TRACE))
    assert.same(true, M.is_level(M.levels.DEBUG))
    assert.same(true, M.is_level(M.levels.INFO))
    assert.same(true, M.is_level(M.levels.WARN))
    assert.same(true, M.is_level(M.levels.ERROR))
    assert.same(true, M.is_level(M.levels.FATAL))
    --
    M.set_level(M.levels.TRACE)
    assert.same(true, M.is_trace())
    assert.same(true, M.is_debug())
    assert.same(true, M.is_level(M.levels.TRACE))
    assert.same(true, M.is_level(M.levels.DEBUG))
    assert.same(true, M.is_level(M.levels.INFO))
    assert.same(true, M.is_level(M.levels.WARN))

    M.set_level(M.levels.INFO)
    assert.same(false, M.is_trace())
    assert.same(false, M.is_debug())
    assert.same(false, M.is_level(M.levels.TRACE))
    assert.same(false, M.is_level(M.levels.DEBUG))
    assert.same(true, M.is_level(M.levels.INFO))
    assert.same(true, M.is_level(M.levels.WARN))

    M.set_level(M.levels.FATAL)
    assert.same(false, M.is_trace())
    assert.same(false, M.is_debug())
    assert.same(false, M.is_level(M.levels.TRACE))
    assert.same(false, M.is_level(M.levels.DEBUG))
    assert.same(false, M.is_level(M.levels.INFO))
    assert.same(false, M.is_level(M.levels.WARN))
    assert.same(false, M.is_level(M.levels.ERROR))
    assert.same(true, M.is_level(M.levels.FATAL))

    M.messages = nil -- clear
  end)

  it("format __tostring", function()
    local t = {
      x = 8,
      __tostring = function(o)
        return 'X:' .. tostring(o.x)
      end
    }
    assert.same('value: X:8', M.format('value: %s', t))
  end)

  it("format toString", function()
    local t = {
      x = 8,
      toString = function(o)
        return 'X:' .. tostring(o.x)
      end
    }
    assert.same('value: X:8', M.format('value: %s', t))
  end)

  -- Goal of ::CALL:: is call a given function to get a value in the fly
  it("format ::CALL:: func ( args ) more", function()
    local func = function(self)
      return tostring(self.x) .. ':' .. tostring(self.y)
    end
    local t = { x = 8, y = 4 }
    local exp = 'v: 8:4 END'
    assert.same(exp, M.format('v: %s %s', '::CALL::', func, '(', t, ')', 'END'))
  end)

  it("format ::CALL:: diff cases", function()
    local func = function(o) return tostring(o) end

    local t = { x = 8, y = 4 }
    local exp = 'v: nil {y = 4, x = 8}'
    -- without () call function without args
    assert.same(exp, M.format('v: %s %s', '::CALL::', func, t))

    local ts = tostring
    local func2 = function(o, next)
      return '[' .. ts(o.x) .. ':' .. ts(o.y) .. ' ' .. ts(next) .. ']'
    end
    exp = 'v: [8:4 nil] %s'
    assert.same(exp, M.format('v: %s %s', '::CALL::', func2, '(', t))

    exp = 'v: [8:4 99] %s'
    assert.same(exp, M.format('v: %s %s', '::CALL::', func2, '(', t, 99))

    exp = 'v: [8:4 nil] 99'
    assert.same(exp, M.format('v: %s %s', '::CALL::', func2, '(', t, ')', 99))

    local func3 = function(o, next) return type(o) .. '|' .. type(next) end

    exp = 'v: nil|nil {y = 4, x = 8} 99'
    assert.same(exp, M.format('v: %s %s', '::CALL::', func3, '()', t, 99))
  end)

  it("format ::CALL:: inspect table return velue", function()
    local func = function(o) return { state = 64, o = o } end

    local t = { x = 8, y = 4 }
    local exp = 'v: {state = 64, o = {y = 4, x = 8}} %s'
    assert.same(exp, M.format('v: %s %s', '::CALL::', func, '(', t))
  end)

  it("format ::CALL:: ret val is not a string", function()
    local func = function() return nil end

    assert.same('v: nil %s', M.format('v: %s %s', '::CALL::', func))

    local func2 = function() return 8 end
    assert.same('v: 8 %s', M.format('v: %s %s', '::CALL::', func2))

    local func3 = function() return {8} end
    assert.same('v: {[1] = 8} %s', M.format('v: %s %s', '::CALL::', func3))
  end)
end)

-- local today = os.date("*t")
-- local filename = string.format("%s-%s-%s.txt", today.year, today.month, today.day)
-- local file = io.open(filename, "r") -- 2022-12-12.txt
