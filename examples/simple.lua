#!/usr/bin/env lua
-- 24-01-2024
-- This is a simple example of using a logger
-- all messages will be written to the /tmp/latest.log file
-- messages with a logging level higher than DEBUG will be output to stdout

local log = require 'alogger'

-- This messages will be ignored.
-- Because out of the box the logger is completely turned off
log.info('First message without setup')
log.debug('Second message without setup')
log.fatal('will also be ignored')


-- configure work to save into file and print into stdout
log.setup({
  save = true,
  level = log.levels.DEBUG,
  appname = 'simple',
  log_dir = '/tmp', -- defualt is "${HOME}/appname/"
  -- log_file = 'latest.log'
  -- (build_log_message)
  -- app_root = '.', -- used to make short source trace-lines from full-paths
  -- do not print messages with DEBUG and TRACE level into StdOut (only to file)
  -- silent_debug = true,
})


log.info('just the one-line message')
log.debug('debug message: %s, %s, %s, %s', 'Hello, Logger!', 16, '32', { 64 })
log.trace('this message will be ignored')

print("\nLogger Status: ", log.status(), "\n")

print('to check the result of the logger use the command')
print('cat '.. tostring(log.get_outfile()))

print('Done.')
