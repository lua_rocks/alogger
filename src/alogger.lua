--
-- base logger implementation with minimal dependency
-- its file contains copy from utils.files (mkdir, dir_exists, etc)
--
-- Dependency:
--   * Global vim at M.notify() for (Optional)
--       - vim.in_fast_event(), - vim.schedule(), - vim.notify
--
-- 19-04-2023 @author Swarg
-- 24-01-2024 Moved into a separate package
--
local M = {}

M._VERSION = 'alogger 0.6.0'
M._URL = 'https://gitlab.com/lua_rocks/alogger'


local inside_vim = vim ~= nil and vim.loop ~= nil

-- logger cofig (it can be part of all plugn|app config like config.logger)
-- WARN: by default the logger is completely disabled(turned off).
-- use the `M.setup(config)` method to configure.
-- This is done intentionally, for example to avoid unnecessary logging during
-- unit tests.
local config = { save = false, print = nil, level = 99 } -- off


local notify_opt = { title = '' } -- the plugin name for nvim
local DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
local LOG_MESSAGE_FORMAT = "[%s] [%-5s] %s:%d: %s\n"

-- Levels idx:         0        1       2       3        4        5
local lvl_names = { "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" }
-- copy of nvim M.levels
M.levels = {
  TRACE = 0, -- in table it has index 1
  DEBUG = 1,
  INFO = 2,
  WARN = 3,
  ERROR = 4,
  FATAL = 5,
}
-- current loglevel used by CondGuard in logger
-- in setup configured by config.level
local LEVEL = M.levels.INFO
local default_nvim_modes = {
  { name = "TRACE", hl = "Comment",    level = M.levels.TRACE }, -- 0
  { name = "DEBUG", hl = "Comment",    level = M.levels.DEBUG }, -- 1
  { name = "INFO",  hl = "None",       level = M.levels.INFO },  -- 2
  { name = "WARN",  hl = "WarningMsg", level = M.levels.WARN },  -- 3
  { name = "ERROR", hl = "ErrorMsg",   level = M.levels.ERROR }, -- 4
  { name = "FATAL", hl = "ErrorMsg",   level = M.levels.FATAL }, -- 5
}
local default_config = {
  -- used only in logger setup, the CondGuard use LEVEL
  level = M.levels.INFO,
  -- by default dont write log-messages to fs
  save = false,
  -- used to make short source trace-lines from full-paths  (build_log_message)
  app_root = '.',   -- vim.fn.stdpath('cache') .. fs.join_path()
  appname = 'logs', -- default subdirectory for logs
  log_dir = nil,    -- M.get_logdir(appname),
  -- full filename to logging messages to fs (Defauil: log_dir+app+latest.log
  -- default will be 'latest.log'
  log_file = nil,

  -- Keep trace and debug messages silent, only write to file
  silent_debug = true,
  -- print = _G.print,
}



if inside_vim then
  default_config.modes = default_nvim_modes
end

--
-- init logger
--
-- Note:
-- Out of the box, without calling this method, the logger is turned off.
-- And completely, and this means that it will not produce any messages at all.
-- That is, no messages will be created for any logging levels at all
-- and will not produce any messages either to the StdOut or into file
--
---@param opts table -- config
---@return table config{save}
function M.setup(opts)
  opts = opts or {}
  -- config = vim.tbl_deep_extend("force", default_config, opts)
  config = M.tbl_deep_copy(default_config)
  M.tbl_override(config, opts)
  notify_opt.title = config.appname or 'logs'

  -- force_setup - to setup saving to files even if save is false (def:lazy setup)
  M.set_save(config.save, config.force_setup) -- init fs to saving if needed

  -- update config lvl from opts
  if not config.level then
    LEVEL = M.levels.INFO
    config.level = LEVEL
  else
    if type(config.level) == 'string' then
      ---@diagnostic disable-next-line: param-type-mismatch
      config.level = M.level_str2i(config.level) -- name_to_idx
    end
    M.set_level(config.level)
  end

  if not config.print then
    config.print = _G.print or print
  elseif type(config.print) ~= 'function' then
    error('config print must be a function')
  end

  return config
end

-- human readable list of supported levels
-- index:lvl_name
function M.get_supported_levels()
  local s = ''
  for k, v in pairs(lvl_names) do
    if k > 1 then s = s .. ', ' end
    s = s .. tostring(k - 1) .. ':' .. v
  end
  return s
end

function M.is_valid_lvl(level)
  return type(level) == 'number' and level >= 0 and level <= M.levels.FATAL
end

function M.get_level()
  return LEVEL
end

function M.get_level_name()
  return lvl_names[LEVEL + 1]
end

---@param level number
function M.set_level(level)
  if M.is_valid_lvl(level) then
    LEVEL = level
  else
    print('[LOGGER][WARN] Illegal Level value: \'' .. tostring(level) .. '\'')
    print('Supports Levels: ' .. M.get_supported_levels())
    if not M.is_valid_lvl(LEVEL) then -- make sure initialized LEVEL is valid
      LEVEL = M.levels.INFO
    end
    print(string.format('Current Level is: %s:%s', LEVEL, M.get_level_name()))
  end
  config.level = LEVEL -- sync
end

--
-- Set the datetime fromat for log messages
-- to disable datetime in the log messages set a empty string ('')
-- to reset to a default value pass a nil value
-- default is '%Y-%m-%d %H:%M:%S'
--
---@param format string|nil
function M.set_date_format(format)
  DATE_FORMAT = format or '%Y-%m-%d %H:%M:%S'
end

function M.set_log_message_format(format)
  LOG_MESSAGE_FORMAT = format or "[%s] [%-5s] %s:%d: %s\n"
end

-- Convert lvlname from string to number
---@param lvl_name string
function M.level_str2i(lvl_name)
  assert(type(lvl_name) == 'string', 'Name expacted as string')
  local lvlnr = tonumber(lvl_name)                 -- '2' --> 2:INFO
  if not M.is_valid_lvl(lvlnr) then
    lvlnr = M.levels[string.upper(lvl_name)] or -1 -- 'INFO' --> 2:INFO
  end
  return lvlnr
end

-- return path to std nvim log dirs
-- Can works outside the vim
function M.get_logdir()
  local logdir
  local appname = config.appname or 'logs'

  if not vim or not vim.fn then -- for testing and works outside the nvim
    logdir = os.getenv('HOME')
    --
  elseif (vim.fn.has "nvim-0.8.0" == 1) then
    logdir = vim.fn.stdpath("log")
  else
    logdir = vim.fn.stdpath("cache")
  end

  if (type(appname) == 'string' and appname ~= '') then
    logdir = M.join_path(logdir, appname)
  end

  return logdir
end

function M.get_outfile()
  return config.log_file
end

function M.get_app_root()
  return config.app_root
end

-- Prepare data for friting log-messages to fs
-- setup log_dir, log_file
-- if save == true -- check is a log-dir exists (mkdir if need)
---@private
---@param force boolean|nil -- force to create the log_dir and the log_file
function M.setup_saving(force)
  config.log_dir = config.log_dir or M.get_logdir()
  config.log_file = config.log_file or 'latest'
  -- resolve log_file to full-name
  if not string.find(config.log_file, M.path_sep, 1, true) then
    config.log_file = string.format("%s/%s.log", config.log_dir, config.log_file)
  end
  -- Lazy directory and log file creation
  if not config.save and not force then
    return
  end

  if not M.ensure_dir_exists(config.log_dir, config.mode) then
    print('[WARN] Cannot create log-directory: "' .. config.log_dir .. '"')
    print('[WARN] Try to create a log file in the current directory')
    -- try to save log to current dir
    -- local fn = config.log_file -- './latest.log'
    local f, err = io.open(config.log_file, 'a') -- check is can write(append) to file
    if f then
      -- how to get full filename? (to show new log_file place)
      io.close(f)
      -- config.log_file = fn
      if not force then -- not override already configured flag "config.save"
        config.save = true
      end
    else
      print("Cannot write to file: " .. tostring(err))
      print('[LOGGER][WARN] Logging will not be written to fs')
      config.save = false
    end
    -- check is can write to configured file
  elseif force then
    local f, err = io.open(config.log_file, 'a') -- check is can write(append) to file
    if f then
      io.close(f)
    else
      print("Cannot write to file: " .. tostring(err))
      print('[LOGGER][WARN] Logging will not be written to fs')
      config.save = false
    end
  end
end

--- Switch do save or not save a log messages to fs
--- on each call with save == true will be re-init saving data (log_file)
---@param save boolean
---@param force boolean|nil
function M.set_save(save, force)
  save = save and true or false -- convert to bool
  assert(type(save) == 'boolean', 'save is boolean')

  config.save = save
  if save or force then
    M.setup_saving(force)
  end
end

---@param on boolean
function M.set_silent_debug(on)
  if on == true or on == false then
    config.silent_debug = on
  end
end

function M.get_silent_debug()
  return config.silent_debug
end

function M.is_saving()
  return config.save == true and config.log_file ~= nil
end

-- setup will be already called (for manual testing via research)
function M.is_initialized()
  if config then
    if next(config) == nil then
      return false -- myTable is empty
    end
    return true
  end
  return false
end

-- readonly!
---@return table
function M.__get_config()
  return config
end

-- Clean latest log from all messages
---@param rotate boolean? if true do rotate (TODO)
---@diagnostic disable-next-line: unused-local
function M.purge(rotate)
  local outfile = M.get_outfile()
  if not outfile then
    outfile = M.join_path(M.get_logdir(), 'latest.log')
  end
  --TODO impl rotate
  local file = io.open(outfile, 'w')
  if (file) then
    io.output(file)
    io.write('')
    io.close(file)
  end
end

--
-- Using the root package name of the current application(namespace),
-- get the root directory of this application
-- to remove full paths from log messages
--
---@param app_ns string
function M.setup_app_rootdir(app_ns)
  local info = debug.getinfo(2, "S")
  if app_ns and app_ns ~= '' and info and info.source then
    local psep = M.path_sep
    local i = string.find(info.source, psep .. app_ns .. psep, 1, true)
    if i then
      local off = info.source:sub(1, 1) == '@' and 2 or 1
      local dir = info.source:sub(off, i)
      config.app_root = dir
      return dir
    end
  end
  return nil
end

--
-- make paths to sources more readable
-- remove specified source root and
-- clears "@./" from the beginning of lines
-- (to provide one result for run tests via busted with diff paths )
--
-- -- @/full/path/to/app/lua/module/file.lua -> module/file.lua
-- -- @test/module_spec.lua -> test/module_spec.lua
--
---@param source string
---@param src_root string
---@return string
function M.mk_short_source(source, src_root)
  local off = 1

  if source then
    if source:sub(1, 3) == '@./' then
      off = 4
    elseif source:sub(1, 1) == '@' then
      off = 2
    end
  end

  if src_root and src_root ~= '' and src_root ~= '.' then
    local i = string.find(source, src_root, off, true)
    if i then
      local j = i + #src_root
      local d0 = string.sub(source, j, j + 4)
      if d0 == '/lua/' or d0 == '/src/' then -- todo win \\
        j = j + 5                            -- skip '/lua/'
      end
      off = j
    end
  end

  if off > 1 and source then
    source = source:sub(off)
  end
  return source or ''
end

--
--
---@param offset number
function M.get_trace(offset)
  local src, lnum = "", 0
  local info = debug.getinfo(2 + offset - 1, "Sl")  -- +1 on each proxy func
  if info and info.source and info.currentline then -- short_src
    lnum = info.currentline
    -- fix deep
    if lnum and lnum == -1 and offset > 1 then
      local info2 = debug.getinfo(2 + offset - 1, "Sl")
      if info2 then info = info2 end
      lnum = info.currentline
    end
    src = M.mk_short_source(info.source, config.app_root)
  end
  return src, lnum
end

--
-- build backtrace from given offset goes up to callers limited by a deep steps
--
---@param offset number?
---@param deep number?(def is 4)
function M.get_trace_path(offset, deep)
  deep = deep or 4
  offset = offset or 0
  local t = {}

  while deep > 0 do
    offset = offset + 1
    local info = debug.getinfo(2 + offset - 1, "Sl") -- +1 on each proxy func
    if not info then
      break
    end
    local src = M.mk_short_source(info.source, config.app_root)
    t[#t + 1] = { src, info.currentline }
    deep = deep - 1
  end

  return t
end

---@param t table
---@return string
function M.trace2str(t, sep)
  local s = ''
  sep = sep or ' '
  for _, tt in ipairs(t) do
    local src, lnum = tt[1], tt[2]
    s = s .. tostring(src) .. ':' .. tostring(lnum) .. sep
  end
  return s
end

---@private
---@param deep number
---@param lvl number
---@param msg string|nil
function M.build_log_message(deep, lvl, msg)
  local date
  if DATE_FORMAT ~= '' then
    date = os.date(DATE_FORMAT)
  else
    date = ''
  end
  -- local info = debug.getinfo(deep or 2, "Sl")       -- +1 on each proxy func
  -- local src, line = "", 0
  -- if info and info.source and info.currentline then -- short_src
  --   line = info.currentline
  --   src = M.mk_short_source(info.source, config.app_root)
  -- end
  local src, line = M.get_trace(deep)
  local lvl_name = lvl_names[lvl + 1] -- in table indexes from 0

  -- "[%s] [%-5s] %s:%d: %s\n",
  return string.format(LOG_MESSAGE_FORMAT,
    date, lvl_name, src, line, (msg or ''))
end

-- Write(Append) message to a config.log_file
--- TODO keep log_file opened for appending ?
---@param message string already built by call M.build_log_message(msg, ...)
function M.save_message(message)
  if message and config.save and config.log_file then
    local file = io.open(config.log_file, "a")
    if not file then
      print('[LOGGER][ERROR] Cannot open file: ', tostring(config.log_file))
      -- config.save = false ?
      return
    end

    io.output(file)
    io.write(message)
    io.close(file)
  end
end

---
--- Show UI notification
--
--  for work outside the nvim print message to StdOut or to a redirected
--  config.print function
--
---@param log_level number
---@param msg string
function M.notify(log_level, msg)
  if msg and log_level and log_level >= LEVEL then
    if inside_vim then -- vim then
      if vim.in_fast_event() then
        vim.schedule(function()
          vim.notify(msg, log_level, notify_opt)
        end)
      else
        vim.notify(msg, log_level, notify_opt)
      end
    elseif config.print then
      -- new line needs for a log file not for a stdout
      if msg:sub(-1, -1) == "\n" then
        msg = msg:sub(1, -2)
      end
      config.print(msg) -- M.build_log_message(4, log_level, msg or '', true))
    end
  end
end

---@param level number
---@param msg string
function M.log(level, msg, ...)
  if LEVEL <= level and level >= 0 then
    local line = M.build_log_message(3, level, M.format(msg, ...))
    M.save_message(line)    -- appender save to file
    if not config.silent_debug or level > M.levels.DEBUG then
      M.notify(level, line) -- appender show in ui or stdout
    end
  end
end

-- Note: not reuse log method for trace,debug etc because it will broke deep idx

function M.trace(msg, ...)
  local level = M.levels.TRACE
  if LEVEL <= level then
    local line = M.build_log_message(3, level, M.format(msg, ...))
    M.save_message(line)
    if not config.silent_debug then
      M.notify(level, line)
    end
  end
end

function M.debug(msg, ...)
  local level = M.levels.DEBUG
  if LEVEL <= level then
    local line = M.build_log_message(3, level, M.format(msg, ...))
    M.save_message(line)
    if not config.silent_debug then
      M.notify(level, line)
    end
  end
end

-- return true if current log level is TRACE
---@return boolean
function M.is_trace()
  return LEVEL <= M.levels.TRACE
end

-- return true if current log level is DEBUG (or lower e.g. TRACE)
---@return boolean
function M.is_debug()
  return LEVEL <= M.levels.DEBUG
end

-- return true if current log level equals or less than given lvl
---@param lvl number
---@return boolean
function M.is_level(lvl)
  return lvl and LEVEL <= lvl
end

function M.info(msg, ...)
  local level = M.levels.INFO
  if LEVEL <= level then
    local line = M.build_log_message(3, level, M.format(msg, ...))
    M.save_message(line)
    M.notify(level, line)
  end
end

function M.warn(msg, ...)
  local level = M.levels.WARN
  if LEVEL <= level then
    local line = M.build_log_message(3, level, M.format(msg, ...))
    M.save_message(line)
    M.notify(level, line)
  end
end

function M.error(msg, ...)
  local level = M.levels.ERROR
  if LEVEL <= level then
    local line = M.build_log_message(3, level, M.format(msg, ...))
    M.save_message(line)
    M.notify(level, line)
  end
end

function M.fatal(msg, ...)
  local level = M.levels.FATAL
  if LEVEL <= level then
    local line = M.build_log_message(3, level, M.format(msg, ...))
    M.save_message(line)
    M.notify(level, line)
  end
end

-------------------------------------------------------------------------------
--       own impl for extends default_cofigs with custom options             --

-- Create full-deep-copy-of table
---@private
---@param tbl table
---@return table new one
function M.tbl_deep_copy(tbl)
  local ret = {}
  if type(tbl) == 'table' then
    for k, v in pairs(tbl) do
      if type(v) == 'table' then
        ret[k] = M.tbl_deep_copy(v)
      elseif v ~= nil then
        ret[k] = v
      end
    end
  end
  return ret
end

-- Check is tbl a not empty table list with number indexes
---@private
function M.tbl_islist(tbl)
  if type(tbl) == 'table' then
    -- or table is list not map
    -- Tests if a Lua table can be treated as an array.
    local count = 0
    for k, _ in pairs(tbl) do
      if type(k) == 'number' then -- check is all 'keys' are numner indexes
        count = count + 1
      else
        return false
      end
    end
    return (count > 0)
  end
  return false
end

--- Extend ret_tbl by tbl_opts.
--- Values of old keys from ret_tbl will be overrided by values from tbl_opts
--- list-table is treated as a overrid value (Not merge lists content)
---@param ret_tbl table (default_config)
---@param tbl_opts table (overriding options)
---@return table modified ret_tbl (extended by tbl_opts)
---@private
function M.tbl_override(ret_tbl, tbl_opts)
  assert(type(ret_tbl) == 'table', 'ret_tbl Expected a table')
  assert(type(tbl_opts) == 'table', 'otbl Expected a table')

  local function is_tbl(t) return type(t) == 'table' end

  if type(tbl_opts) == 'table' then
    for k, v in pairs(tbl_opts) do
      local retv = ret_tbl[k]
      -- Can merge two map-tables, if neither of them is a list
      -- Only merge empty tables or tables that are not a list
      if (is_tbl(v) and not M.tbl_islist(v)) and
          (is_tbl(retv) and not M.tbl_islist(retv)) then -- can_merge
        M.tbl_override(retv, v)
      else
        ret_tbl[k] = v -- override old value with new one(from otbl
      end
    end
  end
  return ret_tbl
end

--
--
---@return string
function M.status()
  local log_file_exists = M.file_exists(config.log_file)
  local silent = ''
  if LEVEL <= M.levels.DEBUG then -- [V]erbose - print debug into StdOut
    silent = config.silent_debug and '[S]' or '[V]'
  end
  local ret = string.format('%d:%s%s, Save: %s, File: %s Existed: %s',
    tostring(M.get_level()),
    tostring(M.get_level_name()), silent,
    tostring(config.save),
    tostring(config.log_file),
    tostring(log_file_exists)
  )
  if DATE_FORMAT == '' then ret = ret .. ' [No Date]' end
  -- config.print used to append log messages to stdout (_G.print)
  if config.print == nil then ret = ret .. '[No print]' end

  return ret
end

--
-------------------- Copied from String Utils -------------------------------

-- dump object value to string
-- tables prints in simple one-line style without newlines
---@param passed table|nil
function M.inspect0(obj, passed)
  if type(obj) == 'table' then
    passed = passed or { counter = 0 }
    if passed[obj] then
      return '<table-' .. tostring(passed[obj]) .. '>'
    end
    passed.counter = (passed.counter or 0) + 1
    passed[obj] = passed.counter
    local s = '{'
    local first = true
    for k, v in pairs(obj) do
      if not first then
        s = s .. ', '
      end
      if type(k) == 'number' then
        k = '[' .. k .. ']'
      elseif type(k) ~= 'string' then
        -- Note inspect shown srting keys starts with digit as ["N*"]
        k = tostring(k)
      end
      s = s .. k .. ' = ' .. M.inspect0(v, passed)
      first = false
    end
    return s .. '}'
  elseif type(obj) == 'string' then
    return '"' .. obj .. '"'
  else
    return tostring(obj)
  end
end

-- improved string formatting aimed at increased stability
-- extended string formating aimed to
-- and the maximum possible display of all passed arguments
-- ignore broken %patterns if no valid places defined via %s then
-- remains varargs appends to end of msg
-- tables shown in one-line style
---@param fmsg string
---@param ... any
function M.format(fmsg, ...)
  fmsg = fmsg or ''
  local off = 1
  local no_places = false
  local i, cnt = 0, select('#', ...)
  while i < cnt do
    i = i + 1
    local val = select(i, ...)
    local t = type(val)
    if t == 'table' then
      if type(val.__tostring) == 'function' then
        val = val.__tostring(val)
      elseif type(val.toString) == 'function' then
        val = val.toString(val)
      else
        val = M.inspect0(val)
      end
    elseif t ~= 'string' then
      val = tostring(val)
      -- string special values
    elseif val == '::BACKTRACE::' then
      local trace = M.get_trace_path(3, 4)
      val = M.trace2str(trace, ' ')
      --
    elseif val == '::CALL::' and i < cnt then
      local func = select(i + 1, ...)
      if type(func) == 'function' then
        i, val = M.function_call(func, i + 1, ...)
        if type(val) == 'table' then
          val = M.inspect0(val)
        else
          val = tostring(val)
        end
      end
    end

    if no_places then
      fmsg = fmsg .. ' ' .. val -- append to end of line of no more  '%s'
    else
      --local p = string.find(fmsg, '%%%a', off, false)
      local p, ch
      local search = true
      -- find next %X replace '%%' to '%'
      while search do
        search = false
        p = string.find(fmsg, '%', off, true)
        if p then
          ch = string.sub(fmsg, p + 1, p + 1) -- std ch is: 's' 'd' 'f'...
          if ch == '%' then                   -- %% -> %
            fmsg = string.sub(fmsg, 1, p - 1) .. string.sub(fmsg, p + 1)
            off = p + 1
            search = true
          end
        end
      end
      -- replace %? by value
      if p then
        local left = string.sub(fmsg, 1, p - 1)
        local right = string.sub(fmsg, p + 2)
        fmsg = left .. val .. right
        off = #left + #val
      else
        no_places = true
        fmsg = fmsg .. ' ' .. val
      end
    end
  end
  return fmsg
end

--------------------------------------------------------------------------------
-- Files Utils
--------------------------------------------------------------------------------

function M.is_os_windows()
  local cwd = os.getenv('PWD')
  if not cwd or string.find(cwd, '\\') then -- paths in win has \\ instedad of /
    return true                             -- win
  end
  return false                              -- unix
  -- return ok_uv and uv and uv.os_uname().version:match('Windows')
end

M.is_win = M.is_os_windows()
M.path_sep = M.is_win and '\\' or '/'

function M.join_path(dir, file)
  dir = dir or ''
  file = file or ''

  local has_left = #dir > 0 and (dir:sub(-1, -1) == M.path_sep)
  local has_right = #file > 0 and (file:sub(1, 1) == M.path_sep)

  if not has_left and not has_right then
    return dir .. M.path_sep .. file
  elseif has_left and has_right then
    return dir:sub(1, -2) .. file
  end

  return dir .. file
end

---@param filename string
function M.file_exists(filename)
  if filename and filename ~= '' then
    local f = io.open(filename, "rb")
    if f and f.close then
      f:close()
      return true
    end
  end
  return false
end

-- is given path a exists directory
---@param path string
function M.is_exists_dir(path)
  if path and path ~= '' then
    -- the system allows you to open a directory for reading, but
    -- does not allow you to read from it
    local f = io.open(path .. '/', "rb")
    if f and f.close then
      f:close()
      return true
    end
  end
  return false
end

--
-- Check is given path is exists if not create it with all subdirs if possible
-- if in the given directory path there is an already existing file with the
-- given subdirectory name - return an error
--
---@param path string
---@param mode number? permissions for mkdir (default 0700)
---@diagnostic disable-next-line: unused-local
function M.ensure_dir_exists(path, mode)
  if M.is_exists_dir(path) then
    return true
  end
  if path then
    local ret = os.execute('mkdir -p ' .. tostring(path))
    -- return M.is_exists_dir(path)
    if ret ~= 0 and ret ~= true then
      error('Cannot Create Directory ' .. tostring(path))
      return false
    end
    return true
  end
end

--------------------------------------------------------------------------------
--                     for debugging and testing
--------------------------------------------------------------------------------

-- logger_debug
-- without args set logger to print all in stdout and do not write to file
--
---@param quiet boolean|nil defualt is false -- mk verbose log
---@param lvl number|nil -- default is Debug log level
---@param save boolean?  -- default is false
function M.fast_setup(quiet, lvl, save, withdate)
  assert(not quiet or type(quiet) == 'boolean')
  assert(not save or type(save) == 'boolean')
  assert(not lvl or type(lvl) == 'number')

  lvl = lvl or M.levels.DEBUG
  quiet = quiet or false -- def verobse
  save = save or false
  withdate = withdate or false

  M.set_level(lvl)
  M.set_save(save)
  M.set_silent_debug(quiet)
  if not config.print then
    config.print = _G.print -- to StdOut
  end

  M.set_date_format(withdate and nil or '')

  -- if turn on for debugging to StdOut without saving to file
  if lvl <= M.levels.DEBUG and not quiet and not save then
    M.notify(LEVEL, M.build_log_message(3, LEVEL, 'Setup Verbose Output'))
  end

  return M --
end

--
-- aliase logger_off
-- for debugging and testing
--
function M.fast_off()
  -- local log = require('env.logger')
  M.set_level(M.levels.FATAL)
  M.set_save(false)
  M.set_silent_debug(true)
  M.messages = nil    -- clear
  config.print = config.prev_print or _G.print
  M.set_date_format() -- restore default
  return M
end

--
-- collect messages from nofiry into this table to use in tests
M.messages = nil
M.dup2stdout = false

local print2mem = function(line)
  M.messages[#M.messages + 1] = line
  if M.dup2stdout then print(line) end
end

---
---@return string?
---@param offset number? from end of collected messages
function M.last_msg(offset)
  offset = offset or 0
  if M.messages then
    local i = #M.messages - offset
    return M.messages[i]
  end
end

--
-- to use debug output in testing
-- dont added datetime into the log messages
--
---@param lvl number?  -- 0 trace 1 debug 2 info ...  default is DEBUG
---@return table this module
---@return table messages
---@param stdout boolean?
function M.fast_log_to_mem(lvl, stdout)
  if lvl == nil then
    lvl = M.levels.DEBUG
  end

  M.messages = {}
  if config.print ~= print2mem then
    config.prev_print = config.print
  end
  config.print = print2mem
  inside_vim = false -- to skip passing the messages into vim.notify
  M.dup2stdout = stdout == true

  M.set_date_format('') -- without-date

  M.fast_setup(false, lvl, false)
  M.notify(LEVEL, M.build_log_message(4, LEVEL, 'Start Logging to Memory'))

  return M, M.messages
end

--
-- to view all state as one table
-- provide the copied tables, not original
--
---@return table
function M.raw_state()
  return {
    level = LEVEL,
    status = M.status(),
    config = M.tbl_deep_copy(config),
    default_config = M.tbl_deep_copy(default_config),
    inside_vim = inside_vim,
    notify_opt = M.tbl_deep_copy(notify_opt),
    date_fmt = DATE_FORMAT,
    msg_fmt = LOG_MESSAGE_FORMAT,
    messages = M.messages,
    module = M.tbl_deep_copy(M), -- ??
    ['_G.print'] = _G.print,
    is_print2mem = config.print == print2mem and print2mem ~= nil,
    print2mem = print2mem,
  }
end

-- used in the format with ::CALL::
---@return number
---@return string?
function M.function_call(func, i, ...)
  assert(type(func) == 'function', 'expect function')
  local next = select(i + 1, ...)
  local val = ''
  local nts = type(next) == 'string'

  if nts and next == '()' then
    return i + 1, func()
  elseif nts and next == '(' then
    local cnt = 0
    -- find end of params for function:  func '(', param1, param2, param3, ')'
    for j = i + 2, select('#', ...) do
      if select(j, ...) == ')' then
        break
      end
      cnt = cnt + 1
    end

    -- TODO find a way to shorten this code
    if cnt == 0 then
      val = func()
    elseif cnt == 1 then
      local a1 = select(i + 2, ...)
      val = func(a1)
    elseif cnt == 2 then
      local a1, a2 = select(i + 2, ...)
      val = func(a1, a2)
    elseif cnt == 3 then
      local a1, a2, a3 = select(i + 2, ...)
      val = func(a1, a2, a3)
    elseif cnt == 4 then
      local a1, a2, a3, a4 = select(i + 2, ...)
      val = func(a1, a2, a3, a4)
    elseif cnt == 5 then
      local a1, a2, a3, a4, a5 = select(i + 2, ...)
      val = func(a1, a2, a3, a4, a5)
    elseif cnt == 6 then
      local a1, a2, a3, a4, a5, a6 = select(i + 2, ...)
      val = func(a1, a2, a3, a4, a5, a6)
    elseif cnt == 7 then
      local a1, a2, a3, a4, a5, a6, a7 = select(i + 2, ...)
      val = func(a1, a2, a3, a4, a5, a6, a7)
    elseif cnt >= 8 then
      local a1, a2, a3, a4, a5, a6, a7, a8 = select(i + 2, ...)
      val = func(a1, a2, a3, a4, a5, a6, a7, a8)
    end
    if cnt > 8 then
      val = val .. ' ..(ignored params:' .. tonumber(cnt - 8) .. ')'
    end

    return (i + 2 + cnt), val -- i+2 for build_line for while-loop with i++
  end
  val = func()

  return i, val
end

return M
