## A Logger

Simple Logger is designed to save log messages into a file, memory or print
into StdOut, or through any user defined function.
Was originally created and developed as a built-in module for logging in
the nvim plugin. With the aim of providing a simple and convenient debugging tool
The first attempts at reuse without luarocks were the reason for all the logger
logic to be concentrated into one file.
With as few external dependencies as possible

Retained the ability to work inside nvim plugins but became more flexible,
allowing you to simultaneously write messages to both a file and standard
output, or through a user-specified function.

Far from ideal and requires revision and improvement, but time-tested and
relatively stable.

Currently, writing messages to a log file is not optimized and causes the file
to be re-opened every time a log message is output.
This is primarily due to the specific application of this logger.
It was created as a lightweight debugging tool when you don't need to
constantly write logs to a file, but only during rare moments of debugging or
testing.


## Features

Has a number of convenient features for use in tests and debugging, such as:

- Convenient, simple and reliable message formatting with multiple input options
- Time and trace in the log messages
- Fast(one-line) configuration to enable output into StdOut without saving into
  log-file. Useful when you need to quickly enable logging in unit tests.
- Collecting messages into memory without writing them to a file on disk
  (To be able to programmatically check the work of your complicated code)
- the ability to view the the logger status in a human-readable and
  machine-friendly mode


## Notes

Out of the box, without calling `setup` method, the logger is turned off.
And completely, and this means that it will not produce any messages at all.
That is, no messages will be created for any logging levels at all
and will not produce any messages either to the StdOut or into file.
This is done intentionally, to avoid unnecessary logging during unit tests.
See [how-to-use](./examples/simple.lua)


## Logger Levels

- TRACE = 0
- DEBUG = 1
- INFO = 2
- WARN = 3
- ERROR = 4
- FATAL = 5

## Requirements

* [Lua] 5.1+ **or** [LuaJIT] 2.0+


## Installation

To install the latest release

```sh
git clone --depth 1 https://gitlab.com/lua_rocks/alogger
cd alogger
luarocks make
```

or

```sh
luarocks install https://gitlab.com/lua_rocks/alogger/-/raw/master/alogger-scm-1.rockspec?ref_type=heads
```

With LuaRocks:
```sh
luarocks install alogger
```

## Warnings

This is a alfa release. Changes can be made in the future.


## Copyright

See [LICENSE file](./LICENSE)

## TODO List

- more flexible configuration for the ability to filter
  (disable or enable logging) for individual lua-modules.
- optimize writing messages to a file (keep handle to log-file open)

